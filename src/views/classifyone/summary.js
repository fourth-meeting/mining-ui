/* eslint-disable */
import { saveAs } from 'file-saver'
import XLSX from 'xlsx'
import XLSXSTYLE from 'xlsx-style'

function datenum(v, date1904) {
    if (date1904) v += 1462;
    var epoch = Date.parse(v);
    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}

function sheet_from_array_of_arrays(list1, list2) {
    var ws = {};
    var range = {
        s: { c: 10000000, r: 10000000 },
        e: { c: 0, r: 0 }
    };

    for (var R=0; R<list1.length; R++) {
        for (var C=0; C<list1[R].length; C++) {
            if (range.s.r > R) range.s.r = R;
            if (range.s.c > C) range.s.c = C;
            if (range.e.r < R) range.e.r = R;
            if (range.e.c < C) range.e.c = C;

            let s = {
                alignment: { horizontal: 'center', vertical: 'center' },
                font: { sz:11, color: {rgb: '555555'} },
            }
            if(R==0) {
                s.font.sz = 22;
                s.font.bold = true
                s.font.color.rgb = '000'
                // s.font.height = 25
            } else if(R==1) {
                s.font.sz = 13
                s.font.color.rgb = '000'
                s.fill = { fgColor: {rgb: 'C5D9F1'}}
            }

            if(R>=list1.length) {
                s.alignment.horizontal = 'left'
                if(C==0) {
                    s.alignment.horizontal = 'left'
                    s.font.sz = 11
                    s.font.bold = true
                }
            } else if(R>=1) {
                s.border = { color: {auth:1}, top:{style:'thin'}, bottom:{style:'thin'}, left:{style:'thin'}, right:{style:'thin'}}  
            }
 



            let v = list1[R][C]
            var cell = { s: s, v: v }

            if (cell.v == null) {
                cell.v = ''
            } else {
                if (typeof cell.v === 'number') cell.t = 'n';
                else if (typeof cell.v === 'boolean') cell.t = 'b';
                else if (cell.v instanceof Date) {
                    cell.t = 'n';
                    cell.z = XLSX.SSF._table[14];
                    cell.v = datenum(cell.v);
                } else cell.t = 's';
            }
            ws[XLSX.utils.encode_cell({ c: C, r: R })] = cell;
        }
    }

    let tab2Rows = list1.length+2
    for (var R1=0, R2=tab2Rows; R2<list2.length+2+list1.length; ++R2, ++R1) {
        for (var C=0; C<list2[R1].length; ++C) {
            if (range.s.r > R2) range.s.r = R2;
            if (range.s.c > C) range.s.c = C;
            if (range.e.r < R2) range.e.r = R2;
            if (range.e.c < C) range.e.c = C;

            let s = {
                alignment: { horizontal: 'center', vertical: 'center' },
                font: { sz:11, color: {rgb: '555555'} }
            }

            if(R1==0) {
                s.font.sz = 22;
                s.font.bold = true
                s.font.color.rgb = '000'
                // s.font.height = 25
            } else if(R1==1 || R1==2) {
                s.font.sz = 12
                s.font.color.rgb = '000'
                s.fill = { fgColor: {rgb: 'C5D9F1'}}
                s.border = { color: {auth:1}, top:{style:'thin'}, bottom:{style:'thin'}, left:{style:'thin'}, right:{style:'thin'}}
            } else if(R1>=2) {
                s.border = { color: {auth:1}, top:{style:'thin'}, bottom:{style:'thin'}, left:{style:'thin'}, right:{style:'thin'}}
            }
            let v = list2[R1][C]
            var cell = { s: s, v: v }

            if (cell.v == null) {
                cell.v = ''
            } else {
                if (typeof cell.v === 'number') cell.t = 'n';
                else if (typeof cell.v === 'boolean') cell.t = 'b';
                else if (cell.v instanceof Date) {
                    cell.t = 'n';
                    cell.z = XLSX.SSF._table[14];
                    cell.v = datenum(cell.v);
                } else cell.t = 's';
            }

            ws[XLSX.utils.encode_cell({ c: C, r: R2 })] = cell;
        }
    }
    if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
    return ws;
}

function Workbook() {
  if (!(this instanceof Workbook)) return new Workbook();
  this.SheetNames = [];
  this.Sheets = {};
}

function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}

function formatJson(filterVal, jsonData) {
    return jsonData.map(v => filterVal.map(j => { return v[j] }))
}

export function export_json_to_excel(list1, list2) {
    list1 = Object.values(list1)
    list1.forEach((one, index) => one.index = index + 1)
    list1[list1.length-1].index = ''
    list2 = Object.values(list2)
    list2.forEach((one, index) => one.index = index + 1)

    const multiHeader1 = [
        ['烧结配矿表', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        ['序号', '物料名', '当前库存', '物料种类', 'TFe', 'CaO', 'SiO2', 'MgO', 'Al2O3', 'TiO2', 'S', 'P', '水分', 'K2O', 'Na2O', 'Zn', 'Pb', 'V2O5', 'MnO', 'FeO', '烧损', '合计成分', '单价', '配比下限', '配比上限', '混矿比例']
    ]
    const filterVal1 = ['index', 'name', 'stock', 'kind', 'TFe', 'CaO', 'SiO2', 'MgO', 'Al2O3', 'TiO2', 'S', 'P', 'water', 'K2O', 'Na2O', 'Zn', 'Pb', 'V2O5', 'MnO', 'FeO', 'loss', 'sum', 'price', 'lower', 'upper', 'proportion']
    let data1= multiHeader1.concat(formatJson(filterVal1, list1))


    let count = list1.length+5
    const multiHeader2 = [
        ['烧结约束条件', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        ['烧结矿化学成分', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '矿物类型', '', '', '脱硫入口SO2浓度', '库存使用天数'],
        ['分类', 'TFe', 'CaO', 'SiO2', 'MgO', 'Al2O3', 'TiO2', 'S', 'P', '碱度', 'K2O', 'Na2O', 'Zn', 'Pb', 'V2O5', 'MnO', '赤铁矿', '磁铁矿', '褐铁矿', '', '']
    ]
    const filterVal2 = ['limit', 'TFe', 'CaO', 'SiO2', 'MgO', 'Al2O3', 'TiO2', 'S', 'P', 'jiandu', 'K2O', 'Na2O', 'Zn', 'Pb', 'V2O5', 'MnO', 'chitk', 'citk', 'hetk', 'strong', 'days']
    let data2 = multiHeader2.concat(formatJson(filterVal2, list2))


    const merges = [
        "A1:Z1", "A"+count+":U"+count, "A"+(count+1)+":P"+(count+1), "Q"+(count+1)+":R"+(count+1), "T"+(count+1)+":T"+(count+2), "U"+(count+1)+":U"+(count+2), 
    ]
    let ws = sheet_from_array_of_arrays(data1, data2);
    if (!ws['!merges']) ws['!merges'] = [];
        merges.forEach(item => ws['!merges'].push(XLSX.utils.decode_range(item)))
        let arr = [{wch:6}]
        for(let i=1; i<25; i++) {
            if(i<=3) {
                arr.push({wch: 11})
            } else if(i>=17) {
                arr.push({wch: 14})
            } else {
                arr.push({wch: 8})
            }
        }
        ws['!cols'] = arr;
        /* add worksheet to workbook */
        let ws_name = "配矿表";
        let wb = new Workbook();
        wb.SheetNames.push(ws_name);
        wb.Sheets[ws_name] = ws;
        let wbout = XLSXSTYLE.write(wb, { bookType: 'xlsx', bookSST: false, type: 'binary' });
        saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), '烧结配矿表.xlsx');
}