import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
    {path: '/', name: 'login', component: () => import ('../views/login.vue')},
    {
        path: '/home',
        name: 'Layout',
        component: () => import ('../views/Layout.vue'),
        children: [
            {path: '', component: () => import ('../views/home.vue')},
            {path: 'sinter', component: () => import ('../views/classifyone/classifyone.vue')},
            {path: 'constraintone', component: () => import ('../views/classifyone/constraint.vue')},
            {path: 'resultsinter', component: () => import ('../views/classifyone/result.vue')},
            {path: 'classifytwo', component: () => import ('../views/classifytwo/classifytwo.vue')},
            {path: 'resulttwo', component: () => import ('../views/classifytwo/result.vue')},
            {path: 'classifythree', component: () => import ('../views/classifythree/classifythree.vue')},
            {path: 'constraintthree', component: () => import ('../views/classifythree/constraint.vue')},
            {path: 'resultthree', component: () => import ('../views/classifythree/result.vue')},
            {path: 'classifyfour', component: () => import ('../views/classifyfour/classifyfour.vue')},
            {path: 'resultfour', component: () => import ('../views/classifyfour/result.vue')},
            {path: 'sorting', component: () => import ('../views/classifyfour/sorting.vue')},
            {path: 'classifythree-config', component: () => import ('../views/classifythree/config.vue')}
        ],
    }
]

let mode = 'history'
const router = new VueRouter({routes})

export default router