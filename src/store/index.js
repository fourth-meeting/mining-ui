import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        resule1: undefined,
        resule2: undefined,
        result4: undefined
    },
    getters: {
        getresule1(state) {
            return state.resule1 ? state.resule1 : false
        },
        getresule2(state) {
            return state.resule2 ? state.resule2 : false
        },
        getResult4(state) {
            return state.result4 ? state.result4 : false
        }
    },
    mutations: {
        // 更新计算数据
        updatedResule1(state, obj) {
            state.resule1 = obj
        },
        updatedResule2(state, obj) {
            state.resule2 = obj
        },
        updateResult4(state, obj) {
            state.result4 = obj
        }
    },
    actions: {},
    modules: {}
})