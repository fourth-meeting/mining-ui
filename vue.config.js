// 后端服务器地址
let url = 'http://localhost:8080'
module.exports = {
    publicPath: './', // 【必要】静态文件使用相对路径
    outputDir: "./dist", //打包后的文件夹名字及路径
    devServer: { // 开发环境跨域情况的代理配置
        proxy: {
            // 【必要】访问自己搭建的后端服务器
            '/api': {
                target: url,
                changOrigin: true,
                ws: true,
                secure: false,
                pathRewrite: {
                    '^/api': 'localhost:8080'
                }
            },
            // 【范例】访问百度地图的API
            // vue文件中使用方法  this.$http.get("/baiduMapAPI/place/v2/search"
            // 最终实际访问的接口为  http://api.map.baidu.com/place/v2/search
            // 遇到以/baiduMapAPI开头的接口便使用此代理
            '/baiduMapAPI': {
                // 实际访问的服务器地址
                target: 'http://api.map.baidu.com',
                //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样客户端和服务端进行数据的交互就不会有跨域问题
                changOrigin: true,
                ws: true, // 是否启用websockets
                secure: false, // 使用的是http协议则设置为false，https协议则设置为true
                // 将接口中的/baiduMapAPI去掉（必要）
                pathRewrite: {
                    '^/baiduMapAPI': ''
                }
            },
        }
    },
     // css相关配置 配置高于chainWebpack中关于css loader的配置
 css: {
  // 是否使用css分离插件 ExtractTextPlugin
  extract: true,
  // 开启 CSS source maps?是否在构建样式地图，false将提高构建速度
  sourceMap: false,
  // css预设器配置项
  loaderOptions: {
    sass: {
      prependData: `
        @import "@/assets/scss/_variable.scss";
      `
    }
  },
  // 启用 CSS modules for all css / pre-processor files.
  modules: false
}
    // configureWebpack: {
    //     resolve: {
    //         alias: {
    //             '@': resolve('src')
    //         }
    //     }
    // }
}